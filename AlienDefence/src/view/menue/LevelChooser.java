package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;
import java.awt.ComponentOrientation;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user,
			String source) {
		setForeground(Color.ORANGE);
		setBackground(Color.BLACK);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;
		setLayout(null);

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		pnlButtons.setBounds(0, 267, 450, 33);
		add(pnlButtons);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.setBackground(Color.BLACK);
		btnNewLevel.setForeground(Color.ORANGE);
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});

		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.setBackground(Color.BLACK);
		btnSpielen.setForeground(Color.ORANGE);
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSpielen_Clicked(alienDefenceController, user);
			}
		});
		pnlButtons.add(btnSpielen);
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgewaehltes Level bearbeiten");
		btnUpdateLevel.setBackground(Color.BLACK);
		btnUpdateLevel.setForeground(Color.ORANGE);
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level loeschen");
		btnDeleteLevel.setBackground(Color.BLACK);
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setBounds(0, 0, 450, 22);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setBackground(Color.BLACK);
		spnLevels.setForeground(Color.BLACK);
		spnLevels.setBounds(0, 22, 450, 245);
		add(spnLevels);

		tblLevels = new JTable();
		tblLevels.setShowVerticalLines(false);
		tblLevels.setBorder(null);
		tblLevels.setFillsViewportHeight(true);
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();

		if (source.equals("Testen")) {
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);

		} else if (source.equals("Leveleditor")) {
			btnSpielen.setVisible(false);
		}
	}

	private void setExtendedState(float centerAlignment) {
		// TODO Auto-generated method stub

	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}

	/**
	 * user choose a level and want to play this level
	 * 
	 * @param alienDefenceController
	 * @param user
	 */
	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, User user) {
		// Level_id des selektierten Elements auslesen
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));

		// gewähltes Level aus der Persistenz holen
		Level level = alienDefenceController.getLevelController().readLevel(level_id);

		// Gameprozess starten
		Thread t = new Thread("GameThread") {

			@Override
			public void run() {

				// Spielaufruf durchführen
				GameController gameController = alienDefenceController.startGame(level, user);
				new GameGUI(gameController).start();

			}
		};
		// Prozess starten
		t.start();
		// Levelauswahlfenster schließen
		this.leveldesignWindow.dispose();
	}
}
